package bellal.weathertoday;

/**
 * Created by balla on 28-05-17.
 */

import android.app.Activity;
import android.content.SharedPreferences;

public class CityPreference {

    SharedPreferences prefs;

    public CityPreference(Activity activity) {
        prefs = activity.getPreferences(Activity.MODE_PRIVATE);
    }

    // dhaka is default city
    public String getCity() {
        return prefs.getString("city", "Dhaka, BD");
    }

    void setCity(String city) {
        prefs.edit().putString("city", city).commit();
    }
}
